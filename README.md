# Deb-Pacman v1.0
[**Click to DOWNLOAD!**](https://gitlab.com/TriVoxel/deb-pacman/uploads/460d83f8711c1ab5e16065e57e7eeabc/deb-pacman-2.0-0.deb)
> **Contents:**
>*  [Overview](#overview)
>*  [Installation](#installation)
>*  [Features](#features)
>    *  [Supported operations](#supported-operations)
>    *  [Supported options](#supported-options)
>*  [Limitations](#limitations)

## News:
Refactoring is completed and all of the original features are now supported. Any user feedback would be appreciated. Deb-Pacman now officially supports Debian, Ubuntu, and OpenSUSE with more package managers planned. The new bash completion support allows for you to type part of a package name and hit the tab key to automatically fill the rest of the package name.

**Refactor progress:** `Beta`
_Minor missing features and basic bash completion support_

## Overview:
Emulates the Archlinux Pacman package manager feel for Debian/Ubuntu and OpenSUSE users who may prefer the style of Pacman over Apt. This program does not require any additional dependencies! Whoot! Don't expect all features to be added because Apt simply doesn't support all Pacman features and some Pacman features would be too tedious to replicate anyways. Casual users should find no trouble with the lack of features as all the most common Pacman functionality is present.

## Installation:
To install via your package manager, download the latest [release](https://gitlab.com/TriVoxel/deb-pacman/-/releases) which will be in the `.deb` file format. To manually install, simply [download the `pacman` file](https://gitlab.com/TriVoxel/deb-pacman/raw/master/pacman?inline=false) and make `pacman` executable. Then, copy it to `/usr/bin/`. Now you can use it normally in the terminal! If you want bash completion support, [download the `bash-completions/pacman` file](https://gitlab.com/TriVoxel/deb-pacman/raw/master/bash-completion/pacman?inline=false) and make it executable. Then, copy it to `/usr/share/bash-completion/completions/` and make sure it does not get renamed. Now you can do `# pacman -S <partial package name>` and hit tab to complete it automatically. For example, `# pacman -S firef` and hit tab will complete to `# pacman -S firefox `. Also works for remove commands.

_Pro tip: Once this is installed, you can install `.deb` packages with `pacman` by using `# pacman -S <option> /path/to/file.deb`_

## Features:

_Pro tip: Use `# pacman -Ru <package(s)>` to remove orphaned packages (basically apt autoremove)! You can also use `-Rns` if you want!_

## Limitations:
Cannot manage repositories and does not support Archlinux-specific package manager features. Any options in the help menu with `(TODO)` next to them are not available yet and are there as a placeholder.

```
 .--.                  Deb-Pacman vBeta
/ _.-' .-.  .-.  .-.   Copyright (C) 2020 Caden Mitchell. Project not
\  '-. '-'  '-'  '-'   officially endorsed by Arch Linux. Deb-Pacman
 '--'                  comes with no warranty. Use at your own risk.
                       This program may be freely redistributed under
                       the terms of the very permissive MIT license.
```

## Disclaimer:
Deb-Pacman is not endorsed in any way by the Arch Linux team, Debian, or Canonical. Deb-Pacman does not come with any warranty and I take no responsibility for any damage that may be caused by the use or misuse of this software. Deb-Pacman is not recommended for OEM deployment or for widespread OS distribution. This is not production software. Recommended for home users and hobbyists. Use at your own risk.